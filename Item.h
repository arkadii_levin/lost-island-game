#pragma once

#include "Player.h"

#include <SFML/Graphics.hpp>
#include <filesystem>

class Item
{
public:
	~Item() = default;
	Item(const Item&) = delete;
	Item& operator=(const Item&) = delete;

	void loadImage(std::filesystem::path path) noexcept(false);
	void draw(sf::RenderWindow& window) noexcept(false);
	void setPosition(float x, float y);
	void setPosition(sf::Vector2u position);
	void setPosition(sf::Vector2f position);

	bool checkForCollision(sf::FloatRect bounds) noexcept(false);
	sf::FloatRect getGlobalBounds() noexcept(false);

protected:
	Item() = default;

protected:
	sf::Texture texture_;
	sf::Sprite sprite_;
};